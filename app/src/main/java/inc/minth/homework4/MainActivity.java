package inc.minth.homework4;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    LinearLayout mainLayout;
    Student student;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainLayout=findViewById(R.id.main_layout);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.menu_add:addNew();
                break;
            case R.id.menu_remove:
                LinearLayout child=findViewById(R.id.detail_layout);
                if(child!=null)
                {
                    mainLayout.removeView(child);
                }
                break;
            case R.id.menu_view:
                viewImageFull();
                break;

        }
        return super.onOptionsItemSelected(item);
    }
    private void viewImageFull()
    {
        Intent intent=new Intent(this,ImgeViewActivity.class);
        
        intent.putExtra("imgData",student.getImagePath());
        startActivity(intent);
    }
    private void addNew()
    {
        Intent intent= new Intent(this,InputActivity.class);
        startActivityForResult(intent,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode)
        {
            case 1:
                display(data);
                break;
        }
    }
    private void display(Intent data)
    {
        View detailLayout=getLayoutInflater().inflate(R.layout.detail_layout,mainLayout,false);
        mainLayout.addView(detailLayout);
        student=(Student) data.getSerializableExtra("data");
        TextView tvName=detailLayout.findViewById(R.id.tvName);
        TextView tvPhone=detailLayout.findViewById(R.id.tvPhone);
        TextView tvClass=detailLayout.findViewById(R.id.tvClass);
        ImageView imageView=detailLayout.findViewById(R.id.imgView);

        tvClass.setText(student.getStudentClass());
        tvName.setText(student.getName());
        tvPhone.setText(student.getPhoneNumber()+"");
        imageView.setImageURI(Uri.parse(student.getImagePath()));

    }

}
