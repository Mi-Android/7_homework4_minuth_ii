package inc.minth.homework4;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.Serializable;

public class InputActivity extends AppCompatActivity {

    EditText edtName;
    EditText edtPhone;
    EditText edtClass;
    Button btnBrowse;
    Button btnRegistration;
    ImageView imageView;

    String imagePath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        edtName=findViewById(R.id.edtStudentName);
        edtClass=findViewById(R.id.edtClass);
        edtPhone=findViewById(R.id.edtPhone);
        btnBrowse=findViewById(R.id.btnBrowseImg);
        imageView=findViewById(R.id.imgView);
        btnRegistration=findViewById(R.id.btnRegistration);

        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registration();
            }
        });

        btnBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browseImage();
            }
        });
    }

    private void browseImage()
    {
        Intent intent=new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent,2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode== RESULT_OK)
        {
            Uri uri=data.getData();
            imagePath=uri.toString();
            imageView.setImageURI(uri);
        }
    }
    private void registration()
    {
        Student student=new Student();
        student.setImagePath(imagePath);
        student.setName(edtName.getText().toString());
        student.setPhoneNumber(Integer.parseInt(edtPhone.getText().toString()));
        student.setStudentClass(edtClass.getText().toString());
        Intent intent=new Intent();
        intent.putExtra("data",(Serializable)student);
        setResult(1,intent);
        finish();
    }



}
