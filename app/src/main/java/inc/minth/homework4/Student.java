package inc.minth.homework4;

import java.io.Serializable;

public class Student implements Serializable
{
    private String name;
    private int phoneNumber;
    private String studentClass;
    private String imagePath;
    public Student(){}
    public Student(String name, int phoneNumber, String studentClass, String imagePath) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.studentClass = studentClass;
        this.imagePath = imagePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(String studentClass) {
        this.studentClass = studentClass;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
