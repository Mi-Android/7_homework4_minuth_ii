package inc.minth.homework4;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class ImgeViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imge_view);
        String imgPath=getIntent().getStringExtra("imgData");
        ImageView imgView=findViewById(R.id.imageView);
        imgView.setImageURI(Uri.parse(imgPath));
    }
}
